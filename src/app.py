
from dotenv import load_dotenv
import os
import logging
from config import create_api

load_dotenv()
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

api = create_api()
user = api.me()



def reply_comment(id, message):
    message = api.update_status(message, id)
    print(f"https://twitter.com/{user.screen_name}/status/{message.id}")

def main():
    counter = int(os.getenv("COUNTER"))
    start = int(os.getenv("START"))
    total_tweet = 0

    for x in range(counter):
        message = f"{os.getenv('MESSAGE')} {x + start}"
        tweet_id = os.getenv("TWEET_ID")

        reply_comment(tweet_id, message)
        total_tweet = total_tweet + 1

    print(f"\ntotal: {total_tweet}")

if __name__ == "__main__":
    main()


