import random
import string

def random_string():
    letters = string.ascii_letters
    random_string = ( ''.join(random.choice(letters) for i in range(10)) )

    return random_string